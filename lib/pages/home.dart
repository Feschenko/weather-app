import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;

var weather, weather5days;
var apiKey = '9e3e466895c9c0f2090d5ea5921f0772';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List _items = [];
  Future<void> readJson() async {
    final String response = await rootBundle.loadString('assets/cities.json');
    final data = await json.decode(response);
    setState(() {
      _items = data;
    });
    for(int i = 0; i < _items.length; i++) {
      _items[i] = _items[i]['name'] + ', ' + _items[i]['country'];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            onPressed: () async {
              await readJson();
              await showSearch(context: context, delegate: Search(_items));
            },
            icon: Icon(Icons.search, color: Colors.white),
          )
        ],
        title: Text('Sunoptic', style: TextStyle(color: Colors.white)),
      ),

      body: SingleChildScrollView(
        child: GeolocationWeather(),
      ),
    );
  }
}

class GeolocationWeather extends StatefulWidget {
  @override
  _GeolocationWeather createState() => _GeolocationWeather();
}

Stream<String> getWeatherStream() async* {
  getData(lat, lon) async{
    var req = await http.get('https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$lon&appid=$apiKey');
    var data = json.decode(req.body);
    return data;
  }

  getData5days(lat, lon) async{
    var req = await http.get('https://api.openweathermap.org/data/2.5/forecast?lat=$lat&lon=$lon&appid=$apiKey');
    var data = json.decode(req.body);
    return data;
  }

  Position getPosition = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  weather = await getData(getPosition.latitude, getPosition.longitude);
  weather5days = await getData5days(getPosition.latitude, getPosition.longitude);
}

class _GeolocationWeather extends State<GeolocationWeather> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
      stream: getWeatherStream(),
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        if (snapshot.hasError) {
          return Container(
            padding: EdgeInsets.only(top: 20),
            alignment: Alignment.center,
            child: TextWidget(text: 'Please check your network connection', color: Colors.redAccent, fontWeight: FontWeight.normal, fontSize: 20),
          );
        }
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Container(
              padding: EdgeInsets.only(top: 20),
              alignment: Alignment.center,
              child: CircularProgressIndicator(
                semanticsLabel: 'Linear progress indicator',
              ),
            );
          case ConnectionState.done:
            return BuildWeather();
          default:
            if (snapshot.data.isEmpty) {
              return Text('No data');
            }
            return Text(snapshot.data);
        }
      },
    );
  }
}

class Search extends SearchDelegate {
  Future<void> getGeolocationWeather(city) async {
    getData(city) async{
      var req = await http.get('https://api.openweathermap.org/data/2.5/weather?q=$city&appid=$apiKey');
      var data = json.decode(req.body);
      return data;
    }
    getData5days(city) async{
      var req = await http.get('https://api.openweathermap.org/data/2.5/forecast?q=$city&appid=$apiKey');
      var data = json.decode(req.body);
      return data;
    }
    weather = await getData(city);
    weather5days = await getData5days(city);
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: Icon(Icons.close),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  String selectedResult = '';

  @override
  Widget buildResults(BuildContext context) {
    if(weather == null) {
      return Container(
        child: Center(
          child: Text('Weather not found'),
        ),
      );
    }

    return Container(
      padding: EdgeInsets.only(top: 10),
      alignment: Alignment.topCenter,
      child: SingleChildScrollView(
          child: BuildWeather(),
      ),
    );
  }

  final List listExample;
  Search(this.listExample);

  List recentList = [];

  @override
  Widget buildSuggestions(BuildContext context) {
    List suggestionList = [];
    query.isEmpty
        ? suggestionList = recentList
        : suggestionList.addAll(listExample.where(
          (element) => element.contains(query),
    ));

    return ListView.builder(
      itemCount: suggestionList.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(
            suggestionList[index],
          ),
          leading: query.isEmpty ? Icon(Icons.access_time) : SizedBox(),
          onTap: () async {
            selectedResult = suggestionList[index];
            await getGeolocationWeather(selectedResult);
            await showResults(context);
          },
        );
      },
    );
  }
}

class BuildWeather extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topCenter,
      margin: EdgeInsets.only(top: 20),
      child: Column(
        children: <Widget>[
          BuiidCurrentWeather(),
          Build4daysWeather(),
        ],
      ),
    );
  }
}

class BuiidCurrentWeather extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(top: 5, bottom: 5, left: 30, right: 30),
      child: Column(
        children: <Widget>[
          Table(
            defaultColumnWidth: FixedColumnWidth(200),
            border: TableBorder.all(width: 0, color: Colors.transparent),
            children: [
              TableRow( children: [
                Column(children:[TextWidget(text: weather['name'], color: Colors.lightBlue, fontWeight: FontWeight.bold, fontSize: 26),]),
              ]),
              TableRow( children: [
                Column(children:[Image.network('http://openweathermap.org/img/wn/${weather['weather'][0]['icon']}@2x.png'),]),
              ]),
              TableRow( children: [
                Column(children:[TextWidget(text: weather['weather'][0]['description'], color: Colors.lightBlue, fontWeight: FontWeight.bold, fontSize: 20),]),
              ]),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 20),
            child: Table(
              defaultColumnWidth: FixedColumnWidth(100),
              border: TableBorder.all(width: 0, color: Colors.transparent),
              children: [
                TableRow( children: [
                  Column(children:[Image.asset('assets/images/temp.png', width: 28, height: 28),]),
                  Column(children:[TextWidget(text: '${(weather['main']['temp'] - 273).round()}°C', color: Colors.lightBlue, fontWeight: FontWeight.normal, fontSize: 18),]),
                ]),
                TableRow( children: [
                  Column(children:[Image.asset('assets/images/wind.png', width: 28, height: 28),]),
                  Column(children:[TextWidget(text: '${weather['wind']['speed']} m/s', color: Colors.lightBlue, fontWeight: FontWeight.normal, fontSize: 18),]),
                ]),
                TableRow( children: [
                  Column(children:[Image.asset('assets/images/humidity.png', width: 28, height: 28),]),
                  Column(children:[TextWidget(text: '${weather['main']['humidity']}%', color: Colors.lightBlue, fontWeight: FontWeight.normal, fontSize: 18),]),
                ]),
                TableRow( children: [
                  Column(children:[Image.asset('assets/images/pressure.png', width: 28, height: 28),]),
                  Column(children:[TextWidget(text: '${weather['main']['pressure']}hPa', color: Colors.lightBlue, fontWeight: FontWeight.normal, fontSize: 18),]),
                ]),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20),
            child: Table(
              defaultColumnWidth: FixedColumnWidth(50),
              border: TableBorder.all(width: 0, color: Colors.transparent),
              children: [
                TableRow( children: [
                  Column(children:[Image.asset('assets/images/tempMin.png', width: 28, height: 28),]),
                  Column(children:[TextWidget(text: '${(weather['main']['temp_min'] - 273).round()}°C', color: Colors.lightBlue, fontWeight: FontWeight.normal, fontSize: 18),]),
                  Column(children:[Image.asset('assets/images/tempMax.png', width: 28, height: 28),]),
                  Column(children:[TextWidget(text: '${(weather['main']['temp_max'] - 273).round()}°C', color: Colors.lightBlue, fontWeight: FontWeight.normal, fontSize: 18),]),
                ]),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class Build4daysWeather extends StatelessWidget {
  List<Widget> weatherWidgets4Days() {
    List<Widget> list = new List();
    for(int i = 8; i < weather5days['list'].length; i += 8) {
      var x, y;
      if(weather5days['list'][i-3]['main']['temp'] - 273 > weather5days['list'][i+3]['main']['temp'] - 273) {
        x = weather5days['list'][i-3]['main']['temp'] - 273;
        y = weather5days['list'][i+3]['main']['temp'] - 273;
      } else {
        y = weather5days['list'][i-3]['main']['temp'] - 273;
        x = weather5days['list'][i+3]['main']['temp'] - 273;
      }
      list.add(new Container(
        color: Colors.white,
        margin: EdgeInsets.all(20),
        padding: EdgeInsets.all(5),
        child: Column(
          children: <Widget>[
            TextWidget(text: ((weather5days['list'][i]['dt_txt']).substring(5, 10)).replaceFirst(RegExp('-'), ' '), color: Colors.lightBlue, fontWeight: FontWeight.bold, fontSize: 22),
            Image.network('http://openweathermap.org/img/wn/${weather5days['list'][i]['weather'][0]['icon']}@2x.png'),
            TextWidget(text: '${y.round()}°    ${x.round()}°C', color: Colors.lightBlue, fontWeight: FontWeight.normal, fontSize: 20),
          ],
        ),
      ),);
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Wrap(
          children: weatherWidgets4Days(),
        ),
      ]
    );
  }
}

class TextWidget extends StatelessWidget {
  final String text;
  final Color color;
  final FontWeight fontWeight;
  final double fontSize;
  const TextWidget({Key key, this.text, this.color, this.fontWeight, this.fontSize}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textDirection: TextDirection.ltr,
      style: TextStyle(
        fontWeight: fontWeight,
        fontSize: fontSize,
        color: color,
      ),
    );
  }
}